angular.module('pratix.controllers', [])

.controller('ContactsCtrl', function($rootScope, $scope, $stateParams, $timeout, $window, XucDirectory) {

  $scope.init = function() {
    if ($stateParams.action == 'search') {
      $scope.showSearch = true;
    }
    else {
      $scope.showSearch = false;
    };
  };

  $scope.init();

  $scope.$on('login', function() {
    console.log('Logged on, get favorites.');
    $scope.getFavorites();
  });

  $scope.$on('searchResultUpdated', function(e) {
    $scope.searchResult = XucDirectory.getSearchResult();
    $scope.$digest();
  });

  $scope.$on('favoritesUpdated', function(e) {
    console.log("FavoritesUpdated");
    $scope.favorites = XucDirectory.getFavorites();
    console.log($scope.favorites);
    $scope.$digest();
  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if (toParams.action == 'search') {
      $scope.showSearch = true;
      $scope.initSearch();
    }
    else {
      $scope.showSearch = false;
    };
  });

  $scope.normalize = function(nb) {
    nb = ""+nb;
    nb = nb.replace(/^\+/,"00");
    return nb.replace(/[ ()\.]/g,"");
  };

  $scope.isEmpty = function(field) {
    if (typeof field != undefined) {
      if (field.length > 0) {
        return true;
      }
    }
    return false;
  };

  $scope.contacts = navigator.contacts;

  $scope.search = function() {
    $scope.showSearch = true;
    Cti.directoryLookUp($scope.search.term);

    if( typeof cordova != 'undefined' ) {
      cordova.plugins.Keyboard.close();
    }

  };

  $scope.toggleSearch = function() {
    $scope.showSearch = !$scope.showSearch;
    if ($scope.showSearch) {
      $scope.initSearch();
    }
  };

  $scope.initSearch = function() {
    $timeout(function() {
      console.log('Setting focus');
      $scope.search.term="";
      $scope.searchResult=[];
      $('#numberTodial').focus();
      if (ionic.Platform.isAndroid()){
         cordova.plugins.Keyboard.show();
      }
    },300);
  };

  $scope.mail = function(address) {
    $window.open('mailto:' + address);
  };

  $scope.sms = function(number) {
    $window.open('sms:' + number);
  };

  $scope.tel = function(number) {
    $window.open('tel:' + number);
  };

  $scope.getFavorites = function() {
    Cti.getFavorites();
  };

  $scope.addFavorite = function(contact) {
    Cti.addFavorite(contact.contact_id, contact.source);
  };

  $scope.removeFavorite = function(contact) {
    Cti.removeFavorite(contact.contact_id, contact.source);
  };

})

.controller('HistoryController', function($scope){
    $scope.history = [];

    $scope.statusImage = function(status) {
        var baseUrl = '/assets/images/';
        if(status == 'emitted') return baseUrl + 'emitted-call.svg';
        else if(status == 'answered') return baseUrl + 'answered-call.svg';
        else if(status == 'missed') return baseUrl + 'missed-call.svg';
        return '';
    };

    $scope.getCallHistory = function() {
        Cti.getUserCallHistory(10);
    };

    $scope.processCallHistory = function(history) {
        $scope.history = history;
        for(var i=0; i<$scope.history.length; i++) {
            var callDetail = $scope.history[i];
            if(callDetail.status == 'emitted')
                callDetail.number = callDetail.dstNum;
            else
                callDetail.number = callDetail.srcNum;
        }
        $scope.$apply();
    };

    $scope.processPhoneStatusUpdate = function(statusUpdate) {
        if(statusUpdate.status == 'AVAILABLE') $scope.getCallHistory();
    };

    $scope.dial = function(number) {
        Cti.dial(number);
    };

    Cti.setHandler(Cti.MessageType.CALLHISTORY, $scope.processCallHistory);
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, $scope.processPhoneStatusUpdate);
    $scope.$on('ctiLoggedOn', $scope.getCallHistory);
});