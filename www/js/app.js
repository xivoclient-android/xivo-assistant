var pratixModule = angular.module('Pratix', ['ionic', 'pratix.controllers', 'pratix.utils',
                                             'xuc.services', 'ui.bootstrap']);

pratixModule.run(function($ionicPlatform, $state, $injector, $storage) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }

    if (typeof $storage.get('xivoAssistant.password') != 'undefined') {
      initCti(
        $storage.get('xivoAssistant.username'),
        $storage.get('xivoAssistant.password'),
        $storage.get('xivoAssistant.xuc'),
        $storage.getObject('xivoAssistant.ssl'),
        $injector);
      $state.go('tab.contacts');
    }

  });
});

pratixModule.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $stateProvider.state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: "templates/tabs.html"
  })
  .state('login', {
      url: '/login?error',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  }).state('about', {
      url: '/about?logged',
        templateUrl: 'templates/about.html',
        controller: "AboutCtrl"
  }).state('tab.contacts', {
    url: '/contacts?action',
    views: {
      'tab-contacts': {
        templateUrl: 'templates/tab-contacts.html',
        controller: 'ContactsCtrl'
      }
    }
  })
  .state('tab.conferences', {
    url: '/conferences',
    views: {
      'tab-conferences': {
        templateUrl: 'templates/tab-conferences.html',
        controller: 'ConfRoomController'
      }
    }
  })
  .state('tab.history', {
    url: '/history',
    views: {
      'tab-history': {
        templateUrl: 'templates/tab-history.html',
        controller: 'HistoryCtrl'
      }
    }
  })
  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/tab-settings.html'
      }
    }
  });

  $urlRouterProvider.otherwise('/login');

  $ionicConfigProvider.tabs.position('top');

})
.controller('NavCtrl', function($scope, $rootScope, $state, $ionicHistory, $ionicSideMenuDelegate) {
  $scope.logged = false;

  $scope.showMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
  $scope.showRightMenu = function () {
    $ionicSideMenuDelegate.toggleRight();
  };
  $scope.settings = function() {
    $state.go('tab.settings');
  };
  $scope.about = function() {
    $state.go('about', {'logged': $scope.logged});
  };

  $scope.$on('ctiLoggedOn', function(e) {
    $scope.logged = true;
    $rootScope.$broadcast('login');
  });

  $scope.login = function() {
    $scope.logged = false;
    $rootScope.$broadcast('logoff');
    $state.go('login');
  };
})

pratixModule.controller('LoginCtrl', function($scope, $state, $injector, $storage, $http, $stateParams) {
  $scope.loginTimer;
  $scope.user = {};
  $scope.error = $stateParams.error;

  $scope.init = function() {
    $scope.user.username = $storage.get('xivoAssistant.username');
    $scope.user.password = $storage.get('xivoAssistant.password');
    $scope.user.xuc = $storage.get('xivoAssistant.xuc');
    try {
      $scope.user.ssl = JSON.parse($storage.getObject('xivoAssistant.ssl'));
    }
    catch(err) {
      $scope.user.ssl = true;
    }
  };

  $scope.init();

  $scope.showLetter = function(event) {
    $("#letterViewer")
        .html(String.fromCharCode(event.keyCode))
        .fadeIn(400, function() {
            $(this).fadeOut(400);
        });
  }

  $scope.login = function(user) {
    $scope.saveCredentials(user);
    initCti(user.username,user.password, user.xuc, user.ssl, $injector);
  };

  $scope.$on('login', function(e) {
    console.log('on login');
    $state.go('tab.contacts');
  });

  $scope.$on('linkDisConnected', function(e) {
    var protocol = $scope.user.ssl ? 'https://' : 'http://';
    var url = protocol + $scope.user.xuc +  '/assets/stylesheets/pages/sample.css';
    console.log("url", url);
    $http.get(url).success(function(response, status, headers, config){
      $state.go('login', {'error': 'Vérifiez votre mot de passe.'});
    }).error(function(err, status, headers, config){
      $state.go('login', {'error': 'Problème de connexion sur le serveur, vérifiez l\'adresse de xuc ou contactez votre administrateur.'})
    });
  });

  $scope.saveCredentials = function(user) {
    $storage.set('xivoAssistant.username', user.username);
    if(user.passwordStore == true) {
      $storage.set('xivoAssistant.password', user.password);
    }
    else {
      if (typeof $storage.get['xivoAssistant.password'] != undefined) {
        $storage.remove('xivoAssistant.password');
      }
    }
    $storage.set('xivoAssistant.xuc', user.xuc);
    $storage.setObject('xivoAssistant.ssl', user.ssl);
  };
})

pratixModule.controller('VoiceMailController',['$scope','XucVoiceMail',function($scope, XucVoiceMail){

  $scope.voiceMail = XucVoiceMail.getVoiceMail();
  $scope.logged = false;

  $scope.$on('login', function(e) {
    $scope.logged = true;
  });

  $scope.$on('logoff', function(e) {
    $scope.logged = false;
  });

  $scope.$on('voicemailUpdated', function(e) {
    $scope.voiceMail = XucVoiceMail.getVoiceMail();
    $scope.$digest();
  });

}]);

pratixModule.controller('ConfRoomController',['$scope','$state','XucConfRoom',
  function($scope, $state, XucConfRoom){
  $scope.confRooms = [];

  $scope.$on('ConfRoomsUpdated', function(e) {
      $scope.confRooms = XucConfRoom.getConferenceRooms();
      console.log('on ConfRoomsUpdated ' + JSON.stringify($scope.confRooms));
      $scope.$digest();
  });

  $scope.getStartTime = function(confRoom) {
      console.log(confRoom.members.length);
      if(confRoom.members.length > 0) {
          return moment(confRoom.startTime).format('HH:mm:ss');
      }
      else {
          return '-';
      }
  };

  $scope.search = function() {
    $state.go('tab.contacts', 'search');
  }
}]);


pratixModule.controller('UserController',['$scope','$ionicPopover', 'XucUser',function($scope, $ionicPopover, xucUser){
  $scope.user = xucUser.getUser();
  $scope.userStatuses = {};
  $scope.userStatus = {};
  $scope.logged = false;

  $scope.$on('login', function(e) {
    $scope.logged = true;
  });

  $scope.$on('logoff', function(e) {
    $scope.logged = false;
  });

  $scope.forwardName = function() {
    if($scope.user.uncFwdEnabled) return('En Renvoi');
    if($scope.user.naFwdEnabled || $scope.user.busyFwdEnabled) {
      var fwdLabel = 'Renvoi /';
      if ($scope.user.naFwdEnabled) {
        fwdLabel = fwdLabel + ' NR';
      }
      if ($scope.user.busyFwdEnabled) {
        fwdLabel = fwdLabel + ' OCC';
      }
      return fwdLabel;
    }
    return ('Aucun renvoi');
  };

  $scope.forwardPopover = function() {
    if($scope.user.uncFwdEnabled) return('En Renvoi sur ' + $scope.user.uncFwdDestination);
    if($scope.user.naFwdEnabled || $scope.user.busyFwdEnabled) {
      var fwdLabel = 'Renvoi /';
      if ($scope.user.naFwdEnabled) {
        fwdLabel = fwdLabel + ' NR ' + $scope.user.naFwdDestination;
      }
      if ($scope.user.busyFwdEnabled) {
        fwdLabel = fwdLabel + ' OCC ' +  $scope.user.busyFwdDestination;
      }
      return fwdLabel;
    }
    return ('Cliquer pour changer');
  };

  $scope.isForwarded = function() {
    return  $scope.user.naFwdEnabled || $scope.user.uncFwdEnabled ||  $scope.user.busyFwdEnabled;
  };

  $scope.$on('userConfigUpdated', function(e) {
    $scope.user = xucUser.getUser();
    $scope.userStatuses = xucUser.getUserStatuses();
    $scope.userStatus = xucUser.getUserStatus();
    $scope.$digest();
  });

  $scope.$on('userStatusUpdated', function(e) {
    $scope.userStatus = xucUser.getUserStatus();
    $scope.$apply();
  });

  $scope.openPresencePopover = function($event) {
    $ionicPopover.fromTemplateUrl('templates/presenceDialog.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.presencePopover = popover;
      $scope.presencePopover.show($event);
    });
  };

  $scope.closePresencePopover = function() {
    console.log('Modal presence dismissed at: ' + new Date());
    $scope.presencePopover.hide();
    $scope.presencePopover.remove();
  };

  $scope.toggleDnd = function() {
    Cti.dnd(!$scope.user.dndEnabled);
  };

}]);

pratixModule.controller('ForwardController',['$scope','$ionicPopover',function($scope, $ionicPopover){
  $scope.updatedUncFw = false;
  $scope.updatedBusyFw = false;
  $scope.updatedNaFw = false;

  $scope.updateUncFwd = function() {
    console.log('uuf');
    $scope.updatedUncFw = true;
  };

  $scope.updateBusyFwd = function() {
    console.log('ubf');
    $scope.updatedBusyFw = true;
  };

  $scope.updateNaFwd = function() {
    console.log('unf');
    $scope.updatedNaFw = true;
  };

  $scope.changeUncFwd = function() {
    Cti.uncFwd($scope.user.uncFwdDestination, $scope.user.uncFwdEnabled);
    $scope.closePopover('changeUncFwd');
  };
  $scope.changeNaFwd = function() {
    Cti.naFwd($scope.user.naFwdDestination, $scope.user.naFwdEnabled);
    $scope.closePopover('changeNaFwd');
  };
  $scope.changeBusyFwd = function() {
    Cti.busyFwd($scope.user.busyFwdDestination, $scope.user.busyFwdEnabled);
    $scope.closePopover('changeBusyFwd');
  };

  $scope.processAndClose = function () {
    $scope.process();
    $scope.close();
  };

  $scope.process = function() {
    if ($scope.updatedBusyFw && $scope.user.busyFwdEnabled) {
      console.log('updBusy');
      $scope.updatedBusyFw = false;
      Cti.busyFwd($scope.user.busyFwdDestination, $scope.user.busyFwdEnabled);
    };
    if ($scope.updatedUncFw && $scope.user.uncFwdEnabled) {
      console.log('updUnc');
      $scope.updatedUncFw = false;
      Cti.uncFwd($scope.user.uncFwdDestination, $scope.user.uncFwdEnabled);
    };
    if ($scope.updatedNaFw && $scope.user.naFwdEnabled) {
      console.log('updNA');
      $scope.updatedNaFw = false;
      Cti.naFwd($scope.user.naFwdDestination, $scope.user.naFwdEnabled);
    };
  };

  $scope.close = function() {
    console.log('close');
    $scope.closePopover('close');
  };

  $scope.$on('popover.hidden', function() {
    console.log('process hiding');
    $scope.process();
  });

  $ionicPopover.fromTemplateUrl('templates/forwardDialog.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };

  $scope.closePopover = function() {
    $scope.popover.hide();
  };

}]);

pratixModule.controller('HistoryCtrl', function($scope) {
  $scope.logs={};

});

pratixModule.controller('AboutCtrl', function($scope, $state, $stateParams, $ionicPlatform, $ionicHistory) {
  $scope.version="";

  $ionicPlatform.ready(function() {
    if (typeof cordova != 'undefined') {
      cordova.getAppVersion.getVersionNumber().then(function (appVersion) {
        $scope.version = appVersion;
      });
    }
    else {
      $scope.version = 'Cordova not available';
    }
  });

  $scope.dismiss = function() {
    console.log($stateParams);
    if ($stateParams.logged == "true") {
      $state.go('tab.contacts');
    }
    else {
      $state.go('login');
    }
  };
});

var initCti = function(username, password, hostAndPort, ssl, injector) {
  Cti.debugMsg = true;
  var xucLink = injector.get("XucLink");
  xucLink.setHostAndPort(hostAndPort);
  xucLink.setHomeUrl("/index.html");
  xucLink.initCti(username,password,0,ssl);
};
