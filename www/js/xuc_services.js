
var xucServices = angular.module('xuc.services', []);

 xucServices.filter('queueTime', function() {
    return function(seconds) {
           if (typeof seconds == "undefined" || seconds == "-")
               return "-";
           else if (seconds < 60)
               return seconds;
           if (seconds < 3600)
               return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("m:ss");
           return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("H:mm:ss");
        };
  });
  xucServices.filter('booleanText', function() {
    return function(bool) {
        if(bool)
            return 'Y';
        else
            return 'N';
    };
  });

xucServices.factory('XucQueue',['$timeout','$rootScope',function($timeout, $rootScope){
    return Xuc.Queue($timeout, $rootScope);
}]);

var Xuc = {};

Xuc.Queue = function($timeout, $rootScope) {
    'use strict';
    var NoLongestWaitTime = "-";
    var updateQueueFrequency = 3;

    var queues = [];
    var loadQueues = function() {
        Cti.getList("queue");
        subscribeToQueueStats();
    };
    var subscribeToQueueStats = function() {
        Cti.subscribeToQueueStats();
    };

    var _queueReplace = function(queueConfig) {
        var replaced = false;
        angular.forEach(queues, function(queue, key) {
            if (queue.id === queueConfig.id) {
                angular.forEach(queueConfig, function(value, property) {
                    queue[property] = value;
                });
                replaced = true;
            }
        });
        return replaced;
    };

    this.onQueueConfig = function(queueConfig) {
        queueConfig.LongestWaitTime = NoLongestWaitTime;
        queueConfig.WaitingCalls = 0;
        var replaced = _queueReplace(queueConfig);
        if (_queueReplace(queueConfig) === false) {
            queues.splice(1, 0, queueConfig);
        }
    };

    this.onQueueList = function(queueList) {
        for (var i = 0; i < queueList.length; i++) {
            this.onQueueConfig(queueList[i]);
        }
        $rootScope.$broadcast('QueuesLoaded');
    };
    this.onQueueStatistics = function(queueStatistics) {
        var updateCounters = function(counters, queue) {
            angular.forEach(counters, function(counter, key) {
                queue[counter.statName] = counter.value;
            });
            if (queue.WaitingCalls === 0) {
                queue.LongestWaitTime = NoLongestWaitTime;
            }
        };
        var queueSelector = function(i) {return queues[i].id === queueStatistics.queueId;};
        if (typeof queueStatistics.queueId === 'undefined') {
            queueSelector = function(i) {return queues[i].name === queueStatistics.queueRef;};
        }
        for (var j = 0; j < queues.length; j++) {
            if (queueSelector(j)) {
                updateCounters(queueStatistics.counters,queues[j]);
            }
        }
        $rootScope.$broadcast('QueueStatsUpdated');
    };

    this.start = function() {
        loadQueues();
    };

    this.updateQueues = function(updateQueueFrequency) {
        for (var i = 0; i < queues.length; i++) {
            if (queues[i].LongestWaitTime >= 0 && queues[i].WaitingCalls !== 0) {
                queues[i].LongestWaitTime += updateQueueFrequency;
            } else {
                queues[i].LongestWaitTime = "-";
            }
        }
    };

    this.updateLongestWaitTime = function() {
        this.updateQueues(updateQueueFrequency);
        $timeout(this.updateLongestWaitTime.bind(this),updateQueueFrequency*1000,false);
    };
    $timeout(this.updateLongestWaitTime.bind(this),updateQueueFrequency*1000,false);

    this.getQueue = function(queueId) {
        for (var j = 0; j < queues.length; j++) {
            if (queues[j].id === queueId) {
                return queues[j];
            }
        }
    };
    this.getQueueByIds = function(ids) {
        var qFilter =  function(queue) {
            return (ids.indexOf(queue.id) >= 0);
        };
        return queues.filter(qFilter);
    };

    this.getQueues = function() {
        return queues;
    };

    this.getQueuesExcept = function(excludeQueues) {
        var acceptQueue = function(queue) {
            for (var j = 0; j < excludeQueues.length; j++) {
                if (excludeQueues[j].id === queue.id) {
                    return false;
                }
            }
            return true;
        };
        return queues.filter(acceptQueue);
    };

    Cti.setHandler(Cti.MessageType.QUEUECONFIG, this.onQueueConfig.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUELIST, this.onQueueList.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, this.onQueueStatistics.bind(this));
    return this;
};
xucServices.factory('XucAgent',
    ['$rootScope','XucQueue','$filter','$timeout',
    function($rootScope, xucQueue, $filter, $timeout){
    return Xuc_Agent($rootScope, xucQueue, $filter, $timeout);
}]);

var Xuc_Agent = function($rootScope, xucQueue, $filter, $timeout) {
    var agents = [];
    var agentStatesWatingForConfig = [];
    var userStatuses = [];
    var agentStateTimer = 2;

    var newAgent = function(id) {
        var agent =  {
            'id' : id,
            'lastName' : '',
            'firstName' : '',
            'queueMembers' : [],
            'state' : 'AgentLoggedOut',
            'status' : '-'
        };
        agents.push(agent);
        return(agent);
    };
    var newAgentStatesWatingForConfig = function(id) {
        var agent =  {
            'id' : id,
            'lastName' : '',
            'firstName' : '',
            'queueMembers' : [],
            'state' : 'AgentLoggedOut',
            'status' : '-'
        };
        agentStatesWatingForConfig.push(agent);
        return(agent);
    };

    var updateAgent = function(agentUpdate,agent) {
        angular.forEach(agentUpdate, function(value, property) {
            agent[property] = value;
        });
    };

    var updateQueueMember = function(queueMember) {
        var agent = this.getAgent(queueMember.agentId) || newAgentStatesWatingForConfig(queueMember.agentId);
        if (queueMember.penalty >= 0) {
            agent.queueMembers[queueMember.queueId] = {};
            agent.queueMembers[queueMember.queueId] = queueMember.penalty;
        }
        else {
            delete agent.queueMembers[queueMember.queueId];
        }
    };

    this.buildMoment = function(since) {
        var start = moment().add('seconds', -(since));
        return {
            'momentStart' : start.format(),
            'timeInState' : moment().countdown(start.clone())
        };
    };

    this.onUserStatuses = function(usrtatuses) {
        angular.forEach(usrtatuses, function (userStatus, keyUserStatus) {
            userStatuses[userStatus.name] = userStatus.longName;
        });
    };
    this.getAgents = function() {
        return agents;
    };


    this.getAgent = function(agentId) {
        for (var j = 0; j < agents.length; j++) {
            if (agents[j].id === agentId) {
                return agents[j];
            }
        }
        return null;
    };
    this.getAndCopyAgentStatesWaitingForConfig = function(agentId) {
        for (var j = 0; j < agentStatesWatingForConfig.length; j++) {
            if (agentStatesWatingForConfig[j].id === agentId) {
                var ag = agentStatesWatingForConfig[j];
                agents.push(ag);
                agentStatesWatingForConfig.splice(j,1);
                return ag;
            }
        }
        return null;
    };
    this.getAgentsNotInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] === 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this.getAgentsInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] !== 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this._getAgentsInQueues = function(filter) {
        var agentsInQueues = [];

        var queues = filter();

        angular.forEach(queues, function(queue, index) {
            angular.forEach(this.getAgentsInQueue(queue.id), function(agent,index){
                if (agentsInQueues.indexOf(agent) < 0) agentsInQueues.push(agent);
            });
        });
        return agentsInQueues;
    };
    this.getAgentsInQueues = function(queueName) {
        var qFilter = function() {
            var queues = xucQueue.getQueues();
            queues = $filter('filter')(queues, { name :queueName});
            return queues;
        };
        return this._getAgentsInQueues(qFilter);
    };

    this.getAgentsInQueueByIds = function(ids) {

        var qFilter = function() {
            return xucQueue.getQueueByIds(ids);
        };
        return this._getAgentsInQueues(qFilter);
    };

    this.onAgentList  = function(agentList) {
        for (var i = 0; i < agentList.length; i++) {
            this.onAgentConfig(agentList[i]);
        }
        angular.forEach(agents, function(agent, index) {
            if(agent.firstName === '') {
                agents.splice(index,1);
            }
        });
        $rootScope.$broadcast('AgentsLoaded');
    };
    this.onAgentConfig = function(agentConfig) {
        var agent = this.getAgent(agentConfig.id) || (getAndCopyAgentStatesWaitingForConfig(agentConfig.id) || newAgent(agentConfig.id));
        updateAgent(agentConfig,agent);
    };

    this.onAgentState = function(agentState) {

        var agent = this.getAgent(agentState.agentId) || newAgentStatesWatingForConfig(agentState.agentId);
        agent.state = agentState.acd ? 'AgentOnAcdCall': agentState.name;
        agent.phoneNb =  agentState.phoneNb || '';
        agent.phoneNb = (agent.phoneNb === 'null' ? '' : agent.phoneNb);
        agent.queues = agentState.queues;
        agent.status = userStatuses[agentState.cause] || '-';
        var mt = this.buildMoment(agentState.since);
        agent.momentStart = mt.momentStart;
        agent.timeInState = mt.timeInState;
        $rootScope.$broadcast('AgentStateUpdated',agent.id);
    };

    this.onQueueMember = function(queueMember) {
        updateQueueMember(queueMember);
        $rootScope.$broadcast('QueueMemberUpdated', queueMember.queueId);
    };

    this.onQueueMemberList = function(queueMemberList) {
        var queueIds = [];
        for (var i = 0; i < queueMemberList.length; i++) {
            updateQueueMember(queueMemberList[i]);
            queueIds[queueMemberList[i].queueId] = true;
        }
        angular.forEach(queueIds, function(done, queueId) {
            $rootScope.$broadcast('QueueMemberUpdated', queueId);
        });
    };

    // timeout
    this.updateAgentStates = function() {

        angular.forEach(agents, function(value, key) {
            agents[key].timeInState = moment().countdown(agents[key].momentStart);
            if (agents[key].state !== "AgentLoggedOut") {
            } else {
                //agents[key].timeInState = moment().add('seconds', 0);
                agents[key].phoneNb = "";
            }
        });
        $timeout(this.updateAgentStates.bind(this), agentStateTimer * 1000, false);
    };
    $timeout(this.updateAgentStates.bind(this), 1000, false);

    this.start = function() {
        console.log('********************** agent service starting *********************');
        Cti.getList("agent");
        Cti.getList("queuemember");
        Cti.getAgentStates();
        Cti.subscribeToAgentEvents();
    };

    this.canLogIn = function(agentId){
        return (this.getAgent(agentId).state === 'AgentLoggedOut');
    };

    this.login = function(agentId) {
        Cti.loginAgent(this.getAgent(agentId).phoneNb,agentId);
    };

    this.canLogOut = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady' || this.getAgent(agentId).state === 'AgentOnPause');
    };
    this.logout = function(agentId) {
        Cti.logoutAgent(agentId);
    };
    this.canPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady');
    };
    this.pause = function(agentId) {
        Cti.pauseAgent(agentId);
    };
    this.canUnPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnPause');
    };
    this.unpause = function(agentId) {
        Cti.unpauseAgent(agentId);
    };
    this.canListen = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnAcdCall' || this.getAgent(agentId).state === 'AgentOnCall' );
    };
    this.listen = function(agentId) {
        Cti.listenAgent(agentId);
    };
    this.updateQueues = function(agentId, updatedQueues) {
        var agent = this.getAgent(agentId);
        var updateQueue = function(queueToUpdate) {
            if(queueToUpdate.penalty !== agent.queueMembers[queueToUpdate.id]) {
                Cti.setAgentQueue(agentId,queueToUpdate.id,queueToUpdate.penalty);
            }
        };
        var updateOrRemoveQueue = function(queueId) {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (updatedQueues[j].id === queueId) {
                  updateQueue(updatedQueues[j]);
                  updatedQueues.splice(j,1);
                  return;
                }
            }
            Cti.removeAgentFromQueue(agentId,queueId);
        };
        var addAgentToQueues = function() {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (typeof agent.queueMembers[updatedQueues[j].id] === 'undefined')
                    Cti.setAgentQueue(agentId,updatedQueues[j].id,updatedQueues[j].penalty);
            }
        };
        angular.forEach(agent.queueMembers, function (penalty, queueId) {
            updateOrRemoveQueue(queueId);
        });
        addAgentToQueues();

    };

    Cti.setHandler(Cti.MessageType.AGENTCONFIG, this.onAgentConfig.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTLIST, this.onAgentList.bind(this));
    Cti.setHandler(Cti.MessageType.USERSTATUSES, this.onUserStatuses.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, this.onAgentState.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBER, this.onQueueMember.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, this.onQueueMemberList.bind(this));

    return this;

};


xucServices.factory('XucGroup',['$rootScope','XucAgent','$filter',function($rootScope, xucAgent, $filter){
    return Xuc_Group($rootScope, xucAgent, $filter);
}]);

var Xuc_Group = function($rootScope, xucAgent, $filter) {
    var MAXPENALTY = 20;

    var groups = [];

    var _onAgentGroupList = function(agentGroups) {
        groups = agentGroups;
    };

    var _getGroups = function() {
        return groups;
    };

    var _getGroup = function(groupId) {
        for (var j = 0; j < groups.length; j++) {
            if (groups[j].id === groupId) {
                return groups[j];
            }
        }
        return null;
    };
    var _getAvailableGroups = function(queueId, penalty) {
        var availableGroups = [];
        var availableAgents = xucAgent.getAgentsNotInQueue(queueId);

        angular.forEach(availableAgents, function(agent, key) {
            var group = _getGroup(agent.groupId);
            if (this.indexOf(group) <0) this.push(_getGroup(agent.groupId));
        }, availableGroups);

        return availableGroups;
    };
    var _getGroupsForAQueue = function(queueId) {
        var queueGroups = [];
        var agents = xucAgent.getAgentsInQueue(queueId);

        var initGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') queueGroups[penalty] = {'penalty' : penalty, 'groups' : []};
            if (!queueGroups[penalty-1] && penalty > 0) initGroup(penalty-1);
        };
        var getGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') initGroup(penalty);
            return queueGroups[penalty];
        };
        var getVGroup = function(groupOfGroup, groupId) {
            for (var j = 0; j < groupOfGroup.groups.length; j++) {
                if (groupOfGroup.groups[j].id === groupId) {
                    return groupOfGroup.groups[j];
                }
            }
            var group = angular.copy(_getGroup(groupId));
            group.nbOfAgents = 0;
            group.agents = [];
            groupOfGroup.groups.push(group);
            return group;
        };

        var addToGroup = function(groupOfGroup, agent) {
            var group = getVGroup(groupOfGroup, agent.groupId);
            group.nbOfAgents = group.nbOfAgents + 1;
            group.agents.push(agent);
        };

        var buildGroups = function() {
            angular.forEach(agents, function (agent, keyAgent) {
                if (typeof agent.queueMembers[queueId] !== 'undefined' && typeof agent.groupId !== 'undefined') {
                    var groups = getGroup(agent.queueMembers[queueId]);
                    addToGroup(groups,agent);
                    if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
                }
            });
        };
        initGroup(0);
        buildGroups();
        return queueGroups;
    };
    var _moveAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _addAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _removeAgentGroupFromQueueGroup = function(groupId, queueId, penalty) {
        Cti.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);
    };
    var _addAgentsNotInQueueFromGroupTo = function(groupId, queueId, penalty) {
        Cti.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);
    };
    var _start = function() {
        Cti.getList("agentgroup");
    };

    Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, _onAgentGroupList);

    return {
        getGroups : _getGroups,
        onAgentGroupList : _onAgentGroupList,
        start : _start,
        getGroup : _getGroup,
        getGroupsForAQueue : _getGroupsForAQueue,
        getAvailableGroups : _getAvailableGroups,
        moveAgentsInGroup : _moveAgentsInGroup,
        addAgentsInGroup : _addAgentsInGroup,
        removeAgentGroupFromQueueGroup: _removeAgentGroupFromQueueGroup,
        addAgentsNotInQueueFromGroupTo: _addAgentsNotInQueueFromGroupTo
    };
};
xucServices.factory('XucConfRoom',['$rootScope',function($rootScope){
    return XucConfRoom($rootScope);
}]);

var XucConfRoom = function($rootScope) {
    var _confRooms = [];

    console.log('XucConfRoom ........ can start');
    Cti.getConferenceRooms();

    var _confRoomLoaded = function(newConfRooms) {
        _confRooms = newConfRooms;
        $rootScope.$broadcast('ConfRoomsUpdated');
    };

    var _getConferenceRooms = function() {
        return _confRooms;
    };
    Cti.setHandler(Cti.MessageType.CONFERENCES, _confRoomLoaded);

    return {
        getConferenceRooms : _getConferenceRooms
    };
};
xucServices.factory('XucVoiceMail',['$rootScope',function($rootScope){
    return XucVoiceMail($rootScope);


}]);

var XucVoiceMail = function($rootScope) {
    var voiceMail = {};
    voiceMail.newMessages = 0;
    voiceMail.waitingMessages = 0;
    voiceMail.oldMessages = 0;


    var _onVoiceMailUpdate = function(voiceMailUpdate) {
        voiceMail = angular.copy(voiceMailUpdate);
        $rootScope.$broadcast('voicemailUpdated');
    };

    var _getVoiceMail = function() {
        return voiceMail;
    };

    Cti.setHandler(Cti.MessageType.VOICEMAILSTATUSUPDATE, _onVoiceMailUpdate);

    return {
        onVoiceMailUpdate : _onVoiceMailUpdate,
        getVoiceMail : _getVoiceMail
    };
};

xucServices.factory('XucUser',['$rootScope',function($rootScope){
    return XucUser($rootScope);
}]);

var XucUser = function($rootScope) {
    var user = {};
    var userStatuses = {};
    var userStatus = {};
    user.userId = -1
    user.fullName = '--------';
    user.naFwdEnabled = false;
    user.naFwdDestination = "";
    user.uncFwdEnabled = false;
    user.uncFwdDestination = "";
    user.busyFwdEnabled = false;
    user.busyFwdDestination = "";

    var _updateForwards = function(userConfig) {
        if(userConfig.naFwdDestination !== null) {
            user.naFwdDestination = userConfig.naFwdDestination;
            user.naFwdEnabled =userConfig.naFwdEnabled;
        }
        if(userConfig.uncFwdDestination !== null) {
            user.uncFwdDestination = userConfig.uncFwdDestination;
            user.uncFwdEnabled =userConfig.uncFwdEnabled;
        }
        if(userConfig.busyFwdDestination !== null) {
            user.busyFwdDestination = userConfig.busyFwdDestination;
            user.busyFwdEnabled =userConfig.busyFwdEnabled;
        }
    };


    var _onUserConfig = function(userConfig) {
        if(userConfig.userId == user.userId || userConfig.userFullname !== null ) {
            if(userConfig.naFwdDestination == null) { userConfig.naFwdDestination = user.naFwdDestination }
            if(userConfig.busyFwdDestination == null) { userConfig.busyFwdDestination = user.busyFwdDestination }
            if(userConfig.uncFwdDestination == null) { userConfig.uncFwdDestination = user.uncFwdDestination }
            user = angular.copy(userConfig);
        }
        _updateForwards(userConfig);
        $rootScope.$broadcast('userConfigUpdated');
    };

    var _onUserStatuses = function(usrtatuses) {
        angular.forEach(usrtatuses, function (userStatus, keyUserStatus) {
            userStatuses[userStatus.name] = userStatus;
        });
    };

    var _onUserStatusUpdate = function(status) {
        userStatus = status.status;
        $rootScope.$broadcast('userStatusUpdated');
    };


    var _getUser = function() {
        return user;
    };

    var _getUserStatus = function() {
        return userStatus;
    };

    var _getUserStatuses = function() {
        return userStatuses;
    };

    Cti.setHandler(Cti.MessageType.USERCONFIGUPDATE, _onUserConfig);
    Cti.setHandler(Cti.MessageType.USERSTATUSES, _onUserStatuses);
    Cti.setHandler(Cti.MessageType.USERSTATUSUPDATE, _onUserStatusUpdate);

    return {
        onUserConfig : _onUserConfig,
        getUser : _getUser,
        getUserStatus : _getUserStatus,
        getUserStatuses : _getUserStatuses
    };
};


xucServices.factory('XucLink',['$rootScope',function($rootScope){
    return XucLink($rootScope);
}]);

var XucLink = function($rootScope) {
    console.log("Creating XucLink");

    var _hostAndPort = "";
    var _homeUrl = "/";

    var _loggedOnHandler = function(event) {
        $rootScope.$broadcast('ctiLoggedOn');
        console.log('XucLink logged on handler done');
    };

    var _linkStatusHandler = function(event) {
        console.log("link status : " + event.status);
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
        }
    };

    var _setHostAndPort = function(hostAndPort) {
        _hostAndPort = hostAndPort;
    };
    var _setHomeUrl = function(homeUrl) {
        _homeUrl = homeUrl;
    };
    var _initCti = function(username, password, phoneNumber, ssl) {
        var wsProto = (ssl === true) ? "wss://" : "ws://";
        var wsurl = wsProto + _hostAndPort + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                                phoneNumber + "&amp;password=" + password;

        console.log("sign in " + username + " " + password + ' to ' + _hostAndPort);
        Cti.WebSocket.init(wsurl, username, phoneNumber);
    };

    Cti.setHandler(Cti.MessageType.LOGGEDON, _loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, _linkStatusHandler);

    return {
        setHostAndPort : _setHostAndPort,
        setHomeUrl : _setHomeUrl,
        initCti : _initCti
    };
};

xucServices.factory('XucDirectory',['$rootScope',function($rootScope){
    return XucDirectory($rootScope);


}]);

var XucDirectory = function($rootScope) {
    var searchResult = [];
    var favorites = [];

    var _onSearchResult = function(result) {
        searchResult = angular.copy(result.entries);
        $rootScope.$broadcast('searchResultUpdated');
    };

    var _onFavorites = function(newFavorites) {
        console.log("onfav " + newFavorites.entries)
        favorites = angular.copy(newFavorites.entries);
        console.log("onfav " + favorites)
        $rootScope.$broadcast('favoritesUpdated');
    }

    var _onFavoriteUpdated = function(update) {
        if (update.action === "Removed" || update.action === "Added") {
            console.log("Favorite updated: ", update)
            searchResult.forEach(function (elem) {
                if (elem.contact_id == update.contact_id && elem.source == update.source) {
                    elem.favorite = ! elem.favorite;
                };
            });
        }
        else {
            console.log("SERVER ERROR: Favorite update failed: ", update);
        };
        _updateFavorites(update);
        $rootScope.$broadcast('searchResultUpdated');
    };

    var _updateFavorites = function(update) {
        console.log("Should update favorites with " + update);
        if (update.action === "Removed") {
            var index = favorites.findIndex(function (elem) {
                return elem.source === update.source && elem.contact_id === update.contact_id;
            });
            if (index > 0) {
                console.log("Removing from favorites ", favorites[index]);
                favorites.splice(index, 1);
            };
        }
        else if (update.action == "Added") {
            var newFavorite = searchResult.find(function (elem) {
                return elem.source == update.source && elem.contact_id == update.contact_id;
            });
            console.log("Adding new favorite ", newFavorite);
            favorites.push(newFavorite);
        };
    };

    var _getSearchResult = function() {
        return searchResult;
    };

    var _getFavorites = function() {
        return favorites;
    };

    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, _onSearchResult);
    Cti.setHandler(Cti.MessageType.FAVORITES, _onFavorites);
    Cti.setHandler(Cti.MessageType.FAVORITEUPDATED, _onFavoriteUpdated);

    return {
        onSearchResult : _onSearchResult,
        getSearchResult : _getSearchResult,
        getFavorites : _getFavorites
    };
};